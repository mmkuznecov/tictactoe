#include <SFML/Graphics.hpp>
#include "Back.hpp"

class Game : private sf::NonCopyable{

    public:

        Game();
        void run();

    private:

        void processEvents();
        void update();
        void render();

        sf::RenderWindow mWindow;

        sf::Font mFont;
        sf::Text mText;
        sf::Text mTitle;

        World mWorld;
        std::array<Player, NUMBER_OF_PLAYERS> mPlayers;
};

Game::Game()

	: mWindow(sf::VideoMode(WINDOW_SIZE.x, WINDOW_SIZE.y), "Just Laba")
	, mFont()
	, mText()
	, mTitle()
	, mWorld(mWindow)
	, mPlayers({ { Player::User, Player::Computer } })
{
	
    mWindow.setVerticalSyncEnabled(true);

	if (!mFont.loadFromFile("Sansation.ttf"))
		throw std::runtime_error("Failed to load font");

	mText.setFont(mFont);
	mText.setStyle(sf::Text::Bold);
	mText.setCharacterSize(20);
	mText.setFillColor(sf::Color::White);
	mText.setPosition(30.f, mWindow.getSize().y - 50.f);
	centerOrigin(mText);

	mTitle.setString("Leeesss goooo");
	mTitle.setFont(mFont);
	mTitle.setStyle(sf::Text::Bold);
	mTitle.setCharacterSize(30);
	mTitle.setFillColor(sf::Color::White);
	mTitle.setPosition(mWindow.getSize().x * 0.5f, 50.f);
	centerOrigin(mTitle);
}

void Game::run(){
	
    while (mWindow.isOpen())
	{
		processEvents();
		update();
		render();
	}
}

void Game::processEvents(){


	sf::Event event;

	while (mWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			mWindow.close();
	}
}

void Game::update(){
	
    
    static bool winner = false;
	static bool tie = false;
	static unsigned index = 0;

	if (tie || winner) return;

	switch (mPlayers[index]){

	case Player::User:
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			const sf::Vector2i& position = sf::Mouse::getPosition(mWindow);

			if (position.x > START_POINT.x &&
				position.y > START_POINT.y &&
				position.x < (START_POINT.x + (DIM*SIZE)) &&
				position.y < (START_POINT.y + (DIM*SIZE)))
			{
				unsigned row = static_cast<unsigned>((position.y - START_POINT.y) / SIZE);
				unsigned col = static_cast<unsigned>((position.x - START_POINT.x) / SIZE);

				if (mWorld.applyMove(Player::User, row, col)) {
					winner = mWorld.isWinner(Player::User);
					if (!winner) {
						index ^= 1;
					}
				}
			}
		}
		break;
	
    
    case Player::Computer:
		if (mWorld.applyAl(Player::Computer)) {
			winner = mWorld.isWinner(Player::Computer);
			if (!winner) {
				index ^= 1;
			}
		}
		break;
	}

	if (winner){
		mText.setString("The Winner: " + std::string((mPlayers[index] == Player::User) ? "Human" : "AI"));
		return;
	}

	tie = mWorld.isFull();

	if (tie){
		mText.setString("No winner");
		return;
	}
}

void Game::render()
{
	mWindow.clear();
	mWorld.draw();
	mWindow.draw(mTitle);
	mWindow.draw(mText);
	mWindow.display();
}