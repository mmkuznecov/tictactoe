#include <SFML/Graphics.hpp>
#include <random>

#pragma once

namespace{

	const sf::Vector2i WINDOW_SIZE(640, 480);
	const unsigned NUMBER_OF_PLAYERS = 2;
	const unsigned DIM = 3;
	const float SIZE = 70.f;
	const sf::Vector2f START_POINT(WINDOW_SIZE.x * 0.5f - DIM * SIZE * 0.5f, WINDOW_SIZE.y * 0.5f - DIM * SIZE * 0.5f);
    
}

enum struct Player : unsigned{

	None,
	User,
	Computer
};

template <typename Resource> static
void centerOrigin(Resource& resource){

	sf::FloatRect bounds = resource.getLocalBounds();
	resource.setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));

}

class Tile : public sf::RectangleShape, private sf::NonCopyable{
    
    public:

        Tile() = default;

        void setOwner(Player player){mOwner = player;}

        Player getOwner() const{return mOwner;}

    private:

        Player mOwner = Player::None;

};

class World : private sf::NonCopyable{
	
    struct Move{

		unsigned x = 0;
		unsigned y = 0;

	};

    public:
        explicit World(sf::RenderTarget& outputTarget);

        bool isFull() const;
        bool isWinner(Player player) const;
        bool applyMove(Player player, sf::Uint32 row, sf::Uint32 column) const;
        bool applyAl(Player player) const;
        void draw();

    private:
        Move minimax() const;
        int minSearch(int level) const;
        int maxSearch(int level) const;

    private:
        sf::RenderTarget& mTarget;
        mutable unsigned mRemain;
        mutable std::array<Tile, DIM * DIM> mTiles;
};

World::World(sf::RenderTarget& outputTarget)
	: mTarget(outputTarget)
	, mTiles()
	, mRemain(DIM * DIM)
{
	sf::Vector2f startPosition(START_POINT);

	for (unsigned i = 0; i < DIM; ++i)
	{
		for (unsigned j = 0; j < DIM; ++j)
		{
			unsigned index = j * DIM + i;

			mTiles[index].setSize(sf::Vector2f(SIZE, SIZE));
			mTiles[index].setPosition(startPosition);
			mTiles[index].setOutlineThickness(2.f);
			mTiles[index].setFillColor(sf::Color::Black);
			mTiles[index].setOutlineColor(sf::Color::White);

			startPosition.x += SIZE;
		}

		startPosition.y += SIZE;
		startPosition.x = START_POINT.x;
	}
}

void World::draw(){

	for (const auto& tile : mTiles)
	{
		mTarget.draw(tile);
	}

}

bool World::applyMove(Player player, sf::Uint32 row, sf::Uint32 column) const{


	unsigned index = row + DIM * column;

	if ((index > mTiles.size()) || (mTiles[index].getOwner() != Player::None) || row >= DIM || column >= DIM){return false;}

	--mRemain;

	mTiles[index].setOwner(player);

	switch (player)
	{
	case Player::User:

		mTiles[index].setFillColor(sf::Color::Blue);
		break;

	case Player::Computer:

		mTiles[index].setFillColor(sf::Color::Yellow);
		break;

	}

	return true;
}

bool World::isFull() const{return (!mRemain);}

bool World::applyAl(Player player) const{

	Move move = minimax();
	return applyMove(player, move.x, move.y);

}

World::Move World::minimax() const{

	static int level = 0;
	int score = std::numeric_limits<int>::max();
	Move move;

	for (unsigned i = 0; i < DIM; i++)
	{
		for (unsigned j = 0; j < DIM; j++)
		{
			unsigned index = j * DIM + i;

			if (mTiles[index].getOwner() == Player::None)
			{
				mTiles[index].setOwner(Player::Computer);
				--mRemain;

				int temp = maxSearch(level);

				if (temp < score)
				{
					score = temp;
					move.x = i;
					move.y = j;
				}

				mTiles[index].setOwner(Player::None);
				++mRemain;
			}
		}
	}

	return move;
}

int World::maxSearch(int level) const{

	if (isWinner(Player::User)) { return 10; }
	else if (isWinner(Player::Computer)) { return -10; }
	else if (isFull()) { return 0; }

	int score = std::numeric_limits<int>::min();

	for (unsigned i = 0; i < DIM; i++)
	{
		for (unsigned j = 0; j < DIM; j++)
		{
			unsigned index = j * DIM + i;

			if (mTiles[index].getOwner() == Player::None)
			{
				mTiles[index].setOwner(Player::User);
				--mRemain;

				score = std::max(score, minSearch(level + 1) - level);

				mTiles[index].setOwner(Player::None);
				++mRemain;
			}
		}
	}

	return score;
}

int World::minSearch(int level) const{

	if (isWinner(Player::User)) {return 10;}
	else if (isWinner(Player::Computer)) {return -10;}
	else if (isFull()) {return 0;}


	int score = std::numeric_limits<int>::max();


	for (unsigned i = 0; i < DIM; i++)
	{
		for (unsigned j = 0; j < DIM; j++)
		{
			unsigned index = j * DIM + i;

			if (mTiles[index].getOwner() == Player::None)
			{
				mTiles[index].setOwner(Player::Computer);
				--mRemain;

				score = std::min(score, maxSearch(level + 1) + level);

				mTiles[index].setOwner(Player::None);
				++mRemain;
			}
		}
	}

	return score;
}

bool World::isWinner(Player player) const{

	// row
	for (unsigned i = 0; i < DIM; ++i)
	{
		bool rowwin = true;
		bool colwin = true;
		for (unsigned j = 0; j < DIM; ++j)
		{
			rowwin &= mTiles[i*DIM + j].getOwner() == player;
			colwin &= mTiles[j*DIM + i].getOwner() == player;
		}
		if (colwin || rowwin)
			return true;
	}

	// diagonal
	bool diagwin = true;
	for (unsigned i = 0; i < DIM; ++i)
		diagwin &= mTiles[i*DIM + i].getOwner() == player;

	if (diagwin)
		return true;

	diagwin = true;
	for (unsigned i = 0; i < DIM; ++i)
		diagwin &= mTiles[i*DIM + (DIM - i - 1)].getOwner() == player;

	return diagwin;
}